* 2016-04-11 (75 tests total +)

railrem (15) = 62
Sanbka (52) = 82
Berkovich (51) = 91

** 11-301
|  № | ФИО           | Дом | Контр | Сем | *Итого* |                   |
|----+---------------+-----+-------+-----+---------+-------------------|
|    | Аглямов       |     |       |     |       0 |                   |
|    | Алимова       |     |       |     |       0 |                   |
| 48 | Алтынбаева    |  89 |       |     |       0 |                   |
|    | Альмухаметова |     |       |     |       0 |                   |
|    | Аристов       |     |       |     |       0 |                   |
|    | Беков         |     |       |     |       0 |                   |
|    | Валиахметов   |     |       |     |       0 |                   |
| 47 | Газизов       |  29 |       |     |       0 |                   |
|    | Герасимова    |     |       |     |       0 |                   |
|    | Дмитриев      |     |       |     |       0 |                   |
|    | Каташев       |     |       |     |       0 |                   |
|    | Костанян      |     |       |     |       0 |                   |
|    | Ломаев        |     |       |     |       0 |                   |
|    | Мансуров      |     |       |     |       0 |                   |
|    | Масалимова    |     |       |     |       0 |                   |
|    | Молоканов     |     |       |     |       0 |                   |
|    | Новиков       |     |       |     |       0 |                   |
|    | Полях         |     |       |     |       0 |                   |
|    | Сафин         |     |       |     |       0 |                   |
|    | Степанов      |     |       |     |       0 |                   |
| 27 | Тарасенко     | 100 |       |     |      20 |                   |
|    | Тимофеева     |     |       |     |       0 |                   |
|    | Фатхуллин     |     |       |     |       0 |                   |
| 24 | Хабетдинов    |  91 |       |     |    18.2 | simplify, groupBy |
|    | Циммерман     |     |       |     |       0 |                   |
|    | Якимова       |     |       |     |       0 |                   |
| 26 | Якупов        | 100 |       |     |      20 |                   |
#+TBLFM: $6=$5*20/100+$4*10/100+$3*20/100

** 11-302
|  № | ФИО          | Дом | Контр | Сем | *Итого* |
|----+--------------+-----+-------+-----+---------|
|    | Абдрахманов  |     |       |     |         |
|    | Ананьева     |     |       |     |         |
|    | Ахметшина    |     |       |     |         |
|    | Бахтияров    |     |       |     |         |
| 20 | Герасимов    |  68 |       |     |         |
|    | Горлачев     |     |       |     |         |
|    | Ермолаев     |     |       |     |         |
|    | Загирова     |     |       |     |         |
|    | Зарипов      |     |       |     |         |
|    | Зубарев      |     |       |     |         |
|  6 | Игнатьев     |  89 |       |     |         |
|    | Касымов      |     |       |     |         |
|    | Кириллов     |     |       |     |         |
|    | Крошечкин    |     |       |     |         |
|    | Мишин        |     |       |     |         |
|    | Мулюков      |     |       |     |         |
|    | Назмутдинова |     |       |     |         |
|    | Нигматуллина |     |       |     |         |
|    | Осипова      |     |       |     |         |
|    | Павлова      |     |       |     |         |
|    | Сайфеев      |     |       |     |         |
|    | Салемгараев  |     |       |     |         |
|    | Светликова   |     |       |     |         |
|    | Хусаинов     |     |       |     |         |
|    | Чегодаев     |     |       |     |         |
|    | Шарипова     |     |       |     |         |
|    | Якбаров      |     |       |     |         |
#+TBLFM: $6=$5*20/100+$4*10/100+$3*20/100

** 11-303
|  № | ФИО          | Дом | Контр | Сем | *Итого* |                                                       |
|----+--------------+-----+-------+-----+---------+-------------------------------------------------------|
|    | Абрамов      |     |       |     |       0 |                                                       |
|    | Богатырев    |     |       |     |       0 |                                                       |
|    | Володарский  |     |       |     |       0 |                                                       |
|    | Галимова     |     |       |     |       0 |                                                       |
|    | Гнеденков    |     |       |     |       0 |                                                       |
|    | Гришина      |     |       |     |       0 |                                                       |
|    | Жарынин      |     |       |     |       0 |                                                       |
| 18 | Зарипова     |  63 |       |     |    12.6 |                                                       |
|    | Иванов       |     |       |     |       0 |                                                       |
|    | Калашникова  |     |       |     |       0 |                                                       |
|    | Купчихина    |     |       |     |       0 |                                                       |
|    | Нуркаев      |     |       |     |       0 |                                                       |
|    | Рахимова     |     |       |     |       0 |                                                       |
|  7 | Сагидулин    |  90 |       |     |       0 | groupBy, simplify, arith                              |
|    | Селезнева    |     |       |     |       0 |                                                       |
|    | Сергеев      |     |       |     |       0 |                                                       |
|    | Соловяненко  |     |       |     |       0 |                                                       |
|    | Солодухина   |     |       |     |       0 |                                                       |
|    | Тагиров      |     |       |     |       0 |                                                       |
|    | Талипов      |     |       |     |       0 |                                                       |
|    | Усманов      |     |       |     |       0 |                                                       |
|    | Файзрахманов |     |       |     |       0 |                                                       |
| 16 | Хадеев       |  75 |       |     |      15 |                                                       |
|    | Харисова     |     |       |     |       0 |                                                       |
|    | Хисамутдинов |     |       |     |       0 |                                                       |
| 19 | Цыганков     |  64 |       |     |    12.8 |                                                       |
|    | Шигабутдинов |     |       |     |       0 |                                                       |
| 14 | Юмаев        | 102 |       |     |    20.4 | groupBy, simplify, dyck, arith, instances of optional |
#+TBLFM: $6=$5*20/100+$4*10/100+$3*20/100

** 11-304
|  № | ФИО          | Дом | Контр | Сем | *Итого* |                          |
|----+--------------+-----+-------+-----+---------+--------------------------|
|    | Абдуллин     |     |       |     |       0 |                          |
|    | Анохин       |     |       |     |       0 |                          |
| 50 | Баймурзин    |  84 |       |     |       0 |                          |
|    | Бойко        |     |       |     |       0 |                          |
|    | Гарифуллин   |     |       |     |       0 |                          |
|    | Гурьева      |     |       |     |       0 |                          |
|    | Динмухаметов |     |       |     |       0 |                          |
|    | Дровняшин    |     |       |     |       0 |                          |
|    | Загулова     |     |       |     |       0 |                          |
|    | Зайнуллина   |     |       |     |       0 |                          |
|    | Каримов      |     |       |     |       0 |                          |
|  8 | Каюмов       | 104 |       |     |    20.8 | groupBy, simplify, arith |
| 22 | Кель         |  90 |       |     |      18 | groupBy, simplify, arith |
|    | Минахметова  |     |       |     |       0 |                          |
| 53 | Минушина     |  44 |       |     |       0 |                          |
|    | Музафаров    |     |       |     |       0 |                          |
|    | Низамов      |     |       |     |       0 |                          |
|    | Нурутдинова  |     |       |     |       0 |                          |
| 49 | Сабитов      |  88 |       |     |       0 |                          |
|    | Султанов     |     |       |     |       0 |                          |
| 54 | Суханаева    |   7 |       |     |     1.4 |                          |
|    | Талкамбаев   |     |       |     |       0 |                          |
| 17 | Фаррахов     |  95 |       |     |      19 | groupBy, simplify        |
|    | Фасхетдинов  |     |       |     |       0 |                          |
| 46 | Шайхразиев   |  55 |       |     |       0 |                          |
#+TBLFM: $6=$5*20/100+$4*10/100+$3*20/100

* 2016-03-28 (75 tests total)

(!) Не объявляйте инстанс Show, напишите свой Show'!

|  id | name                   | errors | failures | tests                                          |
|-----+------------------------+--------+----------+------------------------------------------------|
|  27 | "TarasenkoKate"        |        |          |                                                |
|  26 | "Ilnur Yakupov"        |        |          |                                                |
|  25 | "Zagirova Elina"       |      1 |        3 | groupBy simplify simplify simplify             |
|  24 | "Rustem Khabetdinov"   |        |          | groupBy simplify simplify simplify             |
|  23 | "Rustavil"             |        |          | groupBy simplify simplify simplify             |
|  22 | "Alexander Kel"        |        |          | groupBy simplify simplify simplify             |
|  20 | "Denis Gerasimov"      |        |          | groupBy simplify simplify simplify             |
|  19 | "Ilya Tsygankov"       |        |          | groupBy simplify simplify simplify             |
|  18 | "Renata Zaripova"      |        |          | groupBy simplify simplify simplify             |
|  17 | "Aydar Farrakhov"      |        |          | groupBy simplify simplify simplify             |
|  16 | "Ayrat Khadeev"        |        |          | groupBy simplify simplify simplify             |
|   6 | "Maxim Ignatiev"       |      1 |        5 | Show' Show' groupBy simplify simplify simplify |
|  15 | "railrem"              |        |          | Show' Show' groupBy simplify simplify simplify |
|  14 | "Ruzal Yumaev"         |        |          | Show' Show' groupBy simplify simplify simplify |
|  12 | "Ildar Sayfeev"        |        |          | Show' Show' groupBy simplify simplify simplify |
|   8 | "Kirill Kayumov"       |        |          | Show' Show' groupBy simplify simplify simplify |
|   9 | "Rinat Salemgaraev"    |        |          | Show' Show' groupBy simplify simplify simplify |
|   7 | "Arthur Sagidulin"     |        |          | Show' Show' groupBy simplify simplify simplify |
|  54 | "Sasha Suhanaewa"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  53 | "Minushina Rina"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  52 | "Sanbka"               |        |          | Show' Show' groupBy simplify simplify simplify |
|  51 | "Berkovich Mira"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  50 | "Vladislav Baimurzin"  |        |          | Show' Show' groupBy simplify simplify simplify |
|  49 | "Almaz Sabitov"        |        |          | Show' Show' groupBy simplify simplify simplify |
|  48 | "Dilyara Altynbaeva"   |        |          | Show' Show' groupBy simplify simplify simplify |
|  47 | "Rim Gazizov"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  46 | "Nail Shaikhraziev"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  45 | "Vladislav Boyko"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  44 | "Himi Shoichi"         |        |          | Show' Show' groupBy simplify simplify simplify |
|  43 | "AdelB"                |        |          | Show' Show' groupBy simplify simplify simplify |
|  42 | "Roman Dmitriev"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  41 | "Rustam Fashetdinov"   |        |          | Show' Show' groupBy simplify simplify simplify |
|  40 | "Сhristina Osipova"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  39 | "Aynur"                |        |          | Show' Show' groupBy simplify simplify simplify |
|  37 | "Nurshat Nizamov"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  35 | "Vadim Safin"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  34 | "AnastasiyaA"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  33 | "Дмитрий Молоканов"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  32 | "Rifat Fatkhullin"     |        |          | Show' Show' groupBy simplify simplify simplify |
|  31 | "Eduard Mansurov"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  30 | "Talkambaev Marsel"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  29 | "Rinat Salemgaraev"    |      2 |        2 | groupBy simplify simplify simplify             |
|  28 | "Alina Harisova"       |        |          | groupBy simplify simplify simplify             |
|  89 | "Gena Mishin"          |        |          | groupBy simplify simplify simplify             |
|  87 | "Dinar Abdullin"       |        |          | groupBy simplify simplify simplify             |
|  86 | "Anna Pavlova"         |        |          | groupBy simplify simplify simplify             |
|  84 | "Камиля Назмутдинова"  |        |          | groupBy simplify simplify simplify             |
|  83 | "Vladimir Alexeev"     |        |          | groupBy simplify simplify simplify             |
|  56 | "Bogdan Volodarskij"   |      1 |        3 | groupBy simplify simplify simplify             |
|  82 | "Novikov Stanislav"    |        |          | groupBy simplify simplify simplify             |
|  81 | "Vitaly Abramov"       |        |          | groupBy simplify simplify simplify             |
|  79 | "Zagit Talipov"        |        |          | groupBy simplify simplify simplify             |
|  78 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
|  77 | "Ruslan Yakbarov"      |        |          | groupBy simplify simplify simplify             |
|  66 | "gerasimova"           |        |          | groupBy simplify simplify simplify             |
|  68 | "Alsou"                |        |          | groupBy simplify simplify simplify             |
|  63 | "Tagirov Albert"       |        |          | groupBy simplify simplify simplify             |
|  62 | "Daniil Polyakh"       |        |          | groupBy simplify simplify simplify             |
|  59 | "Константин Ермолаев"  |        |          | groupBy simplify simplify simplify             |
|  58 | "Arthur Tsimmerman"    |        |          | groupBy simplify simplify simplify             |
|  55 | "ilhamkasymov"         |        |          | groupBy simplify simplify simplify             |
|  64 | "Oleggorru"            |        |          | groupBy simplify simplify simplify             |
|  67 | "Rafael Aglyamov"      |        |          | groupBy simplify simplify simplify             |
|  60 | "Руслана Рус"          |        |          | groupBy simplify simplify simplify             |
|  57 | "DianaMasalimova"      |        |          | groupBy simplify simplify simplify             |
|  92 | "Ayrat Mulyukov"       |        |          | groupBy simplify simplify simplify             |
|  91 | "Maxim"                |        |          | groupBy simplify simplify simplify             |
|  90 | "bazinko"              |        |          | groupBy simplify simplify simplify             |
|  98 | "AnastasiyaA"          |        |          | groupBy simplify simplify simplify             |
|  97 | "TarasenkoKate"        |        |          | groupBy simplify simplify simplify             |
|  94 | "Kirill Kayumov"       |        |          | groupBy simplify simplify simplify             |
|  96 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
|  95 | "Ilnur Yakupov"        |        |          | groupBy simplify simplify simplify             |
| 104 | "Talkambaev Marsel"    |        |          | groupBy simplify simplify simplify             |
| 103 | "Almukhametova Albina" |        |          | groupBy simplify simplify simplify             |
| 101 | "Gulnaz Sharipova"     |        |          | groupBy simplify simplify simplify             |
| 105 | "Sasha Suhanaewa"      |        |          | groupBy simplify simplify simplify             |
| 111 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 106 | "Sasha Stepanov"       |        |          | groupBy simplify simplify simplify             |
| 110 | "Tansu Svetlikova"     |        |          | groupBy simplify simplify simplify             |
| 109 | "Nurshat Nizamov"      |        |          | groupBy simplify simplify simplify             |
| 108 | "Vladislav Boyko"      |        |          | groupBy simplify simplify simplify             |
| 107 | "Aydar Farrakhov"      |        |          | groupBy simplify simplify simplify             |
| 114 | "TarasenkoKate"        |        |          | groupBy simplify simplify simplify             |
| 113 | "Ahmetshina"           |        |          | groupBy simplify simplify simplify             |
| 112 | "Руслана Рус"          |        |          | groupBy simplify simplify simplify             |
| 124 | "Ahmetshina"           |        |          | groupBy simplify simplify simplify             |
| 120 | "Maxim"                |        |          | groupBy simplify simplify simplify             |
| 117 | "Rim Gazizov"          |        |          | groupBy simplify simplify simplify             |
| 122 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 121 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 118 | "Sasha Suhanaewa"      |        |          | groupBy simplify simplify simplify             |
| 116 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
| 115 | "Vladislav Boyko"      |        |          | groupBy simplify simplify simplify             |
|-----+------------------------+--------+----------+------------------------------------------------|

* 2016-03-03 (31 tests total)

| PR id | Surname/Name             | Mark | Percent | Failures | Errors | Failer            |                  |         |      |                               |
|-------+--------------------------+------+---------+----------+--------+-------------------+------------------+---------+------+-------------------------------|
|    87 | Abdullin      Dinar      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    81 | Abramov       Vitaly     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    43 | AdelB         ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    67 | Aglyamov      Rafael     |    1 |   16.67 |        6 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    83 | Alexeev       Vladimir   |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /68,/ | Alsou         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    48 | Altynbaeva    Dilyara    |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /.34/ | AnastasiyaA   ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|  /39/ | Aynur         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /50,/ | Baimurzin     Vladislav  |    4 |   66.67 |        1 |      2 | primeSum          | isPrime          |         |      |                               |
| /90,/ | bazinko       ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    51 | Berkovich     Mira       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
| /45./ | Boyko         Vladislav  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    93 | Chegodaev     Denis      |    5 |   83.33 |        1 |      1 | n!!               |                  |         |      |                               |
|    42 | Dmitriev      Roman      |    3 |   50.00 |        3 |      0 | primeIsNotSlow    | n!!              |         |      |                               |
|    59 | Ermolaev      Konstantin |    4 |   66.67 |        2 |      3 | primeSum          | isPrime          |         |      | Внимательно изучайте тесты!   |
| /.17/ | Farrakhov     Aydar      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /41/ | Fashetdinov   Rustam     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /32/. | Fatkhullin    Rifat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    47 | Gazizov       Rim        |    1 |   16.67 |       10 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    20 | Gerasimov     Denis      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    66 | gerasimova    ?          |    3 |   50.00 |        0 |      5 | primeSum          | isPrime          | 1hw2    |      |                               |
|    28 | Harisova      Alina      |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    44 | Himi???       Shoichi??? |    4 |   66.67 |        3 |      0 | isPrime           | n!!              |         |      |                               |
|     6 | Ignatiev      Maxim      |    3 |   50.00 |        2 |      1 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    55 | Kasymov       Ilham      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|   /8/ | Kayumov       Kirill     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /22/ | Kel           Alexander  |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    24 | Khabetdinov   Rustem     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    16 | Khadeev       Ayrat      |    3 |   50.00 |        1 |      2 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    21 | lvelapel      ?          |    4 |   66.67 |       -- |     -- | ?                 |                  |         |      |                               |
|    31 | Mansurov      Eduard     |    4 |   66.67 |        6 |      0 | primeSum          | isPrime          |         |      |                               |
|    78 | Mariya        Z???       |    3 |   50.00 |        2 |      1 | primeSum          | isPrimeIsNotSlow |         |      | Внимательно изучайте тесты!   |
|    57 | Masalimova    Diana      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    91 | Maxim         ?          |    3 |   50.00 |        4 |      0 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    53 | Minushina     Rina       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    89 | Mishin        Gena       |    3 |   50.00 |        3 |      3 | primeSum          | isPrime          | 1hw2    |      |                               |
| /33/. | Molokanov     Dmitry     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    92 | Mulyukov      Ayrat      |    4 |   66.67 |        7 |      0 | n!!               | 1hw2             |         |      |                               |
|    84 | Nazmutdinova  Kamilya    |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    37 | Nizamov       Nurshat    |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    82 | Novikov       Stanislav  |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    64 | Oleggorru     ?          |    2 |   33.33 |        3 |      0 | isPrimeIsNotSlow  | isPrime          | n!!     |      |                               |
| /40./ | Osipova       Christina  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    86 | Pavlova       Anna       |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    62 | Polyakh       Daniil     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    15 | railrem       ?          |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    88 | Rus           Ruslana    |    3 |   50.00 |        3 |      3 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    60 | Rus           Ruslana    |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    23 | Rustavil      ?          |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
| /49,/ | Sabitov       Almaz      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    35 | Safin         Vadim      |    3 |   50.00 |        5 |      0 | primeSum          | isPrime          | 1hw2    |      |                               |
|     7 | Sagidulin     Arthur     |    0 |    0.00 |       17 |      5 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|     9 | Salemgaraev   Rinat      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    29 | Salemgaraev   Rinat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    52 | Sanbka        ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    12 | Sayfeev       Ildar      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    46 | Shaikhraziev  Nail       |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    38 | Stepanov      Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
| /54./ | Suhanaewa     Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    63 | Tagirov       Albert     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    79 | Talipov       Zagit      |    2 |   33.33 |        1 |      4 | isPrimeIsNotSlow  | isPrime          | 1hw2    |      | Не изменяйте название модуля! |
|  /30/ | Talkambaev    Marsel     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    27 | Tarasenko     Kate       |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    58 | Tsimmerman    Arthur     |    3 |   50.00 |        3 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    19 | Tsygankov     Ilya       |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    56 | Volodarskij   Bogdan     |    2 |   33.33 |        1 |      3 | isPrimeIsNotSlow  | n!!              | 1hw2    |      |                               |
| /77,/ | Yakbarov      Ruslan     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /.26/ | Yakupov       Ilnur      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    14 | Yumaev        Ruzal      |      |    0.00 |       -- |     -- | AccessDenied      |                  |         |      |                               |
|    25 | Zagirova      Elina      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    18 | Zaripova      Renata     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
#+TBLFM: $4=100*$3/6;%.2f

